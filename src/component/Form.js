import React, { Component } from 'react'
import './form.css'
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import validator from 'validator'
export class Form extends Component {


  constructor(props) {
    super(props)

    this.state = {
      title: '',
      category: '',
      image: '',
      description: '',
      price: '',
      rating: '',
      titleError: '',
      categoryError: '',
      imageError: '',
      descriptionError: '',
      priceError: '',
      ratingError: ''
    }
  }
  titlechange = (event) => {
    this.setState({
      title: event.target.value
    })
  }
  urlchange = (event) => {
    this.setState({
      image: event.target.value
    })
  }
  categorychage = (event) => {
    this.setState({
      category: event.target.value
    })
  }

  descriptionchange = (event) => {
    this.setState({
      description: event.target.value
    })
  }

  pricechange = (event) => {
    this.setState({
      price: event.target.value
    })
  }
  ratechange = (event) => {
    this.setState({
      rate: event.target.value
    })
  }
  finalSubmit = (event) => {
    const { title, category, description, image, rating, price } = this.state
    if (!validator.isAlpha(title)) {
      this.setState({
        titleError: "please enter valid Title"
      })
    } else {
      this.setState({
        titleError: ''
      })
    }
    if (!validator.isAlpha(category)) {
      this.setState({
        categoryError: "please enter valid category"
      })
    } else {
      this.setState({
        categoryError: ''
      })
    }
    if (!validator.isAlpha(description)) {
      this.setState({
        descriptionError: "please enter valid description"
      })
    } else {
      this.setState({
        descriptionError: ''
      })
    } 
    if (!validator.isURL(image)) {
      this.setState({
        imageError: "please enter valid URL"
      })
    } else {
      this.setState({
        imageError: ''
      })
    } if ((!validator.isNumeric(rating))&&(rating>=5.0)) {
      this.setState({
        ratingError: "please enter valid rating"
      })
    } else {
      this.setState({
        rating: ''
      })
    } if (!validator.isNumeric(price)) {
      this.setState({
        priceError: "please enter valid price"
      })
    } else {
      this.setState({
        priceError: ''
      })
    }
    event.preventDefault()
    this.props.getdata(this.state)
  }

  render() {
    return (
      <div className='container'>
        <form className='form' onSubmit={this.finalSubmit}>
          <div className='input-container'>
            <label>Title </label>
            <input type='text' placeholder='Title of product' onChange={this.titlechange} />
            <span>{this.state.titleError.length>0 ? <label className='error-message'> {this.state.titleError} </label> : <label>&nbsp;</label>}</span>

          </div>
          <div className='input-container'>
            <label>Image </label>
            <input type='text' placeholder='image url' onChange={this.urlchange} />
            <span>{this.state.imageError.length>0 ? <label className='error-message'> {this.state.imageError} </label> : <label>&nbsp;</label>}</span>
          </div>
          <div className='input-container'>
            <label>Category </label>
            <input type='text' placeholder='category of product' onChange={this.categorychage} />
            <span>{this.state.categoryError.length>0 ? <label className='error-message'> {this.state.categoryError} </label> : <label>&nbsp;</label>}</span>
          </div>
          <div className='input-container'>
            <label>Description </label>
            <input type='text' placeholder='description of product' onChange={this.descriptionchange} />
            <span>{this.state.descriptionError.length>0 ? <label className='error-message'> {this.state.categoryError} </label> : <label>&nbsp;</label>}</span>
            
          </div>
          <div className='input-container'>
            <label> price</label>
            <input type='text' placeholder='Price of product' onChange={this.pricechange} />
            <span>{this.state.priceError.length>0 ? <label className='error-message'> {this.state.priceError} </label> : <label>&nbsp;</label>}</span>
          </div>
          <div className='input-container'>
            <label> Rating</label>
            <input type='text' placeholder='Rating of product' onChange={this.ratechange} />
            <span>{this.state.ratingError.length>0 ? <label className='error-message'> {this.state.ratingError} </label> : <label>&nbsp;</label>}</span>

          </div>

          <button type='submit' className='submitbutton'> submit </button>

        </form>

      </div>
    )
  }
}

export default Form