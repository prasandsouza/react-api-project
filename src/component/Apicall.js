import React, { Component } from 'react'
import axios from 'axios'
import Loader from './Loader'
import './apicall.css'
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import MappingProduct from './MappingProduct'
import Individualproducts from './Individualproducts'
import Form from './Form'




class Apicall extends Component {
    constructor(props) {
        super(props)
        this.appState = {
            Loading: 'loading',
            loaded: 'loaded',
            error: 'error',
        }
        this.state = {
            status: this.appState.Loading,
            data: [],
        }
        console.log(this.props);
    }

    getdata = (userdata) =>{
        // console.log(userdata)
        const dataarray = {...userdata,id:this.state.data.length+1}
        console.log(dataarray)
        this.setState({
            data : [...this.state.data,dataarray]
        },()=>{
            console.log(this.state.data)
        })
    }

    componentDidMount = () => {
        axios.get('https://fakestoreapi.com/products')
            .then((event) => {
                console.log(event.data)
                this.setState({
                    status: this.appState.loaded,
                    data: event.data
                })
            })
            .catch((err) => {
                console.log(err);
                this.setState({
                    status: this.appState.error
                })
            })

    }
    render() {
        return (

            <div className='mainContainer'>
                <h1 className='heading'> Our products</h1>




                {this.state.status === this.appState.Loading ?
                    <Loader message="LOADING..." /> :
                    ''}

                {this.state.status === this.appState.error ?
                    <div className='error-message'> <h1>  <b>Error occured while fetching!... </b></h1> </div> :
                    ''}

                <div className='product-container'>
                    {this.state.status === this.appState.loaded ?
                        <div>{this.state.data.length === 0 ? <h1 className='error-message'> NO PRODUCT AVAILABLE!!!!!! </h1> :
                            <BrowserRouter>
                                <Routes>
                                    <Route path="/" element={<MappingProduct data={this.state.data} />} />
                                    <Route path="/:id" element={<Individualproducts />} />
                                    <Route path="/addProductForm" element={<Form getdata={this.getdata} />} />
                                </Routes>
                            </BrowserRouter>
                        }</div> : ''}
                </div>

            </div>
        )
    }
}

export default Apicall