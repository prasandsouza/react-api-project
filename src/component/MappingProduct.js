import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './apicall.css';

export class MappingProduct extends Component {
    constructor(props) {
        super(props)
        this.appState = {
            loading: "loading",
            loaded: 'loaded',
            error: 'error'
        }
        this.state = {
            data: this.props.data
        }
    }
    render() {
        console.log(this.state.data);
        const { data } = this.state
        return (
            <div>
            <Link to='/addProductForm'>
                <button className='adding-button'> Add new products</button>
            </Link>
                {data.map((products) => {
                    return (
                        <Link to={`/${products.id}`}>
                            <div className='IndividualProduct'>
                                <p className='ProductCategory'> {products.category} </p>
                                <img src={products.image} className='ProductImage' alt='productimage'/>
                                <h2 className='Producttitle'> {products.title}</h2>
                                <p className='ProductDescription'> {products.description}</p>
                                <div>
                                    <p className="productsrate"> {products.rating.rate} &#9733;  </p>
                                    <p className="productCount">  (out of {products.rating.count} review)</p>
                                </div>
                                <p className='ProductPrice'> &#36; {products.price} </p>
                            </div>
                        </Link>
                    )
                })}
            </div>

        )
    }
}

export default MappingProduct