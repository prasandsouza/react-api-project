import React, { Component } from 'react'
import './loadercss.css'
export class Loader extends Component {
    render() {
        return (
            <div className='loader'>
                <div className="lds-ring">
                    {this.props.message}
                    <div></div><div></div><div></div><div></div></div>
            </div>
        )
    }
}

export default Loader