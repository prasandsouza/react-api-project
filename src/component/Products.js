import React from 'react'
import { BrowserRouter, Link, Route, Routes, Switch } from 'react-router-dom';

function Products(props) {
  const { products } = props;
  const { category, description, id, image, price, rating, title } = products
  return (

    <div className='IndividualProduct'>
      <p className='ProductCategory'> {category} </p>
      <img src={image} className='ProductImage' />
      <h2 className='Producttitle'> {products.title}</h2>
      <p className='ProductDescription'> {products.description}</p>
      <div>
        <p className="productsrate"> {products.rating.rate} &#9733;  </p>
        <p className="productCount">  (out of {products.rating.count} review)</p>
      </div>
      <p className='ProductPrice'> &#36; {products.price} </p>

    </div>


  )
}

export default Products