
import { Link, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import Loader from './Loader'
import './apicall.css'
const ProductDetailsPage = () => {

    const appstate = {
        loading: 'loading',
        loaded: 'loaded',
        error: 'error',
    }
    const params = useParams()
    const { id } = params
    const [productDetails, setProductDetails] = useState([])
    const [isLoading, setIsLoading] = useState(appstate.loading)

    useEffect(() => {
        
        const url = `https://fakestoreapi.com/products/${id}`
        fetch(url)
            .then(data => data.json())
            .then(resp => {
                console.log(resp)
                setIsLoading(appstate.loaded)
                return setProductDetails(resp)
            })
            .catch((error) => {
                setIsLoading(appstate.error)

            })
    }, [])

    return (
        <div> 

            {isLoading === appstate.loading ? <div> <Loader /> </div> : ''}
            {isLoading === appstate.error ? <h1 className='error-message'> Error while loading data </h1> : ''}
            {isLoading === appstate.loaded ?
                <div className='IndividualProduct'>
                    <h1 className='Producttitle'>{productDetails.title}</h1>
                    <img className='ProductImage' src={productDetails.image} />
                    <p className='ProductCategory'>category: {productDetails.category}</p>
                    <p className='ProductDescription'>{productDetails.description}</p>
                    <p className='ProductPrice'> Price: <span>${productDetails.price}</span></p>
                </div>
                : ''
            }
        </div>
    )










}

export default ProductDetailsPage;
